﻿namespace NetCoreExamples
{
    public interface IRunnable
    {
        void Run();
    }
}